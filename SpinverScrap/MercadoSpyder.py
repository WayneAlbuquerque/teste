import scrapy

class MercadoSpider(scrapy.Spider):
    name = 'Mercado'

    #lista de urls que o spider vai mandar requests
    start_urls = ['https://lista.mercadolivre.com.br/carro#D[A:carro]']

    #funcao de callback padrao do primeiro request
    def parse(self, response):
        
        links = response.xpath('//ol[re:test(@id,"searchResults")]/li//a/@href').getall()
        
        for link in links:
            #Geracao de request para cada link na lista
            yield scrapy.Request(
                link,
                callback=self.pegaAnuncio
            )

        proxima = response.css('.pagination__container a::attr(href)').getall()[-1] 
        
        yield scrapy.Request(
            proxima,
            callback=self.parse
        )

    def pegaAnuncio(self,response):
        anuncio = response.css('main header h1::text').get()[3:-2]
        preco = "R$"+response.css('span.price-tag-fraction::text').get()

        yield{
            'anuncio': anuncio,
            'preco': preco
        }
